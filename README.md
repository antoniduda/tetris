# Tetris
Tetris application created as a part of university code. It relies on usb to uart bridge
to display the resulting game on the computer screen using `display.py`

Some of the code is based on: [this](https://codalogic.com/blog/2023/01/07/Pico-Assembly-Programming) blogpost by Pete Cordell
which is a great introduction to compiling and running asm code on Raspberry Pi pico.
